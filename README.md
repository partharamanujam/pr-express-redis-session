# pr-express-redis-session

Express middleware for cookie-sessions backed by REDIS store

## Philosophy

Simple & tested integration of express sessions store backed by redis

## Installation

```bash
$ npm install pr-express-redis-session
```

## APIs

The module exports express-middleware function cookieRedisSessionMiddleware(redisStoreConfig, cookieSessionConfig)

Refer to the individual module documentation for config-options:
* [Redis-Store Options](https://github.com/tj/connect-redis#options)
* [Cookie-Session Options](https://github.com/expressjs/session#options)

## Usage

Refer to test [server.js](https://gitlab.com/partharamanujam/pr-express-redis-session/blob/master/test/server.js) implementation for usage details.

## Test

```bash
$ npm install # inside pr-express-redis-session
$ npm test
```
