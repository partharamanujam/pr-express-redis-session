"use strict";

var http = require("http"),
    Redis = require("ioredis"),
    express = require("express"),
    cookieSessionMiddleware = require("../index");


var app, server,
    redisConfig = {
        "client": new Redis(),
        "prefix": "testsession:"
    },
    sessionConfig = {
        "name": "test-cookie",
        "secret": "this-is-not-a-secret",
        "resave": false,
        "rolling": true,
        "saveUninitialized": false,
        "unset": "destroy",
        "cookie": {
            "path": "/",
            "httpOnly": true,
            "secure": false,
            "maxAge": 5000
        }
    };

// setup express
app = express();
app.use(cookieSessionMiddleware(redisConfig, sessionConfig)); // redis-cookie-session
app.get("/hello", function getHello(req, res) {
    res.end("hello");
});
app.post("/value",
    function createValue(req, res) {
        req.session.value = 1;
        res.end();
    }
);
app.put("/value",
    function incrementValue(req, res) {
        req.session.value++;
        res.end();
    }
);
app.get("/value",
    function getValue(req, res) {
        res.json({
            "value": req.session.value || 0 // eslint-disable-line no-magic-numbers
        });
    }
);

// setup http-server
server = http.createServer(app);

// export
module.exports = server;