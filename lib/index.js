"use strict";

var session = require("express-session"), // eslint-disable-line no-mixed-requires
    RedisStore = require("connect-redis")(session);

function extendSession() {
    return function extendSessionEveryUse(req, res, next) {
        req.session.touch();

        return next();
    };
}

function cookieSession(redisStoreConfig, cookieSessionConfig) {
    var opts = Object.assign({}, cookieSessionConfig);

    opts.store = new RedisStore(redisStoreConfig);

    return session(opts);
}

function cookieRedisSessionMiddleware(redisStoreConfig, cookieSessionConfig) {
    return [cookieSession(redisStoreConfig, cookieSessionConfig), extendSession()];
}

// exports
module.exports = cookieRedisSessionMiddleware;